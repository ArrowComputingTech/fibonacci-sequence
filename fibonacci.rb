#!/home/hz/.rbenv/shims/ruby

def fibs_rec(x)
  fib = x > 1 ? fibs_rec(x-1) : [1]
  fib << fib[-1] + (fib[-2].nil? ? 0 : fib[-2])
end

def fibs(x)
  return 0 if x <= 1
  total = [0,1]
  (x - 2).times do |n|
    total << total[n] + total[n+1]
  end
  return total
end

puts fibs(6)
puts fibs(3)
puts fibs_rec(15)
puts
puts fibs_rec(3)

